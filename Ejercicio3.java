/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciospolotic;

import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese un numero y presione enter, luego ingrese otro numero");
        
        int num1 = sc.nextInt();
        int num2 = sc.nextInt();
        float resultado = 0;
        
        System.out.println("Presione: \n S para sumar \n R para restar \n M para multiplicar \n D para dividir");
        
        String op = sc.next().toUpperCase();
        
        if ("S".equalsIgnoreCase(op)){
        resultado = num1+num2;
          }
        if ("R".equalsIgnoreCase(op)){
        resultado = num1-num2;
          }
        if ("M".equalsIgnoreCase(op)){
        resultado = num1*num2;
          }
        if ("D".equalsIgnoreCase(op)){
        resultado = (float)num1/num2;
          }
        
        System.out.println("El resultado es " + resultado);
        
        
        
    }
    
}
