/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciospolotic;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese la cantidad de pesos a cambiar para obtener cotizaciones");
        
        float pesos = sc.nextFloat();
        
        float usd = pesos / 231.68f;
        float euro = pesos / 250.69f;
        float gua = pesos * 31f;
        float real = pesos / 46.81f;
        
        System.out.printf("Usd = %.2f%nEuro = %.2f%nGuarani = %.2f%nReal = %2f%n", usd, euro, gua, real);
       
    }
    
}
