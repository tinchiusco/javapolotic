/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciospolotic;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese la estatura de 3 personas");
        
        float persona1 = sc.nextFloat();
        float persona2 = sc.nextFloat();
        float persona3 = sc.nextFloat();
        float promedio = (persona1 + persona2 + persona3)/3;
        
        System.out.println("El promedio es de " + promedio);
        
        
        
    }
    
}
