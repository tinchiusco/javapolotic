/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejerciciospolotic;

import java.util.Locale;
import java.util.Scanner;


/**
 *
 * @author tinch
 */
public class Ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Locale.setDefault(Locale.ENGLISH);
        
        
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        System.out.println("Ingrese la edad de la primera persona");
        
        int edad1 = sc.nextInt();
        
        System.out.println("Ingrese la edad de la segunda persona");
        
        int edad2 = sc.nextInt();
        
        int aux = edad1;
        
        edad1 = edad2;
        edad2 = aux;
        
        System.out.println("El ingreso de las edades invertidas son los numeros " + edad1 + " y " + edad2);
        
        
        
    }
    
}
